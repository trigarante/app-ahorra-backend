import {Request, Response} from 'express';
import {getRepository} from 'typeorm';
import {ClienteAppView} from '../../entity/cliente/views/ClienteAppView';

export default class ClienteController {
    static async getById(req: Request, res: Response) {
        const clienteRepository = getRepository(ClienteAppView);
        const cliente = await clienteRepository.findOne(req['id']);

        if (cliente) {
            cliente.cp = +cliente.cp;
            res.status(200).json(cliente);
        } else {
            return res.status(404).json({
                msg: 'Cliente no encontrado'
            });
        }
    }
}
