import {Request, Response} from 'express';
import * as express from 'express';
import jwtConfig from '../../config/JWTconfig';
import * as bodyParser from 'body-parser';
import * as jwt from 'jsonwebtoken';
import {ClienteAuth} from '../../entity/login/ClienteAuth';
import {getRepository} from 'typeorm';

const app = express();
app.set('key', jwtConfig.pass);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

export default class AuthController {
    static async login(req: Request, res: Response) {
        try {
            let contrasenaApp = req.body.contrasenaApp;
            const curp = req.body.curp;

            if (!contrasenaApp || !curp) {
                return res.status(400).json('Faltan credenciales');
            }

            const clienteRepository = getRepository(ClienteAuth);
            const cliente = await clienteRepository.findOne({contrasenaApp: contrasenaApp, curp: curp});

            if (!cliente) {
                return res.status(404).json({
                    msg: 'Usuario no encontrado'
                });
            } else {
                const token = jwt.sign({id: cliente.id, ac: cliente.acceso}, app.get('key'), {
                    expiresIn: '1d'
                });
                return res.status(200).json({
                    token: token,
                    msg: 'Loggeo exitoso'
                });
            }
        } catch (e) {
            console.log(e);
            return res.status(500).json({
                ok: false,
                msg: 'Error'
            });
        }
    };

    static async updatePass (req: Request, res: Response) {
        try {
            const updateRepository = getRepository(ClienteAuth);
            const cliente = await updateRepository.findOne(req['decode'].id);

            if (!cliente) {
                return res.status(404).json({
                    ok: false,
                    msg: 'No se encontro ningun registro con el id proporcionado'
                });
            }

            cliente.contrasenaApp = req['newPass'];
            cliente.acceso = 1;

            await updateRepository.save(cliente);
            const token = jwt.sign({id: cliente.id}, app.get('key'), {
                expiresIn: '1d'
            });
            res.status(200).json({
                token: token,
                msg: 'Registro actualizado',
            });
        } catch (e) {
            console.log(e);
            return res.status(500).json({
                ok: false,
                msg: 'Error'
            });
        }
    };
}
