import {Request, Response} from 'express';
import {getRepository} from 'typeorm';
import {RegistroAppView} from '../../entity/registro/views/RegistroAppView';

export default class RegistroController {
    static async getByIdCliente(req: Request, res: Response) {
        const registroRepository = getRepository(RegistroAppView);
        const registros = await registroRepository.find({idCliente: req['id']});

        if (registros) {
            res.status(200).json(registros);
        } else {
            return res.status(404).json({
                msg: 'Este cliente no tiene registros en el sistema'
            });
        }
    }

    static async downloadPdf (req: Request, res: Response) {
        var data = req.params.base64;
        try {
            var url = `https://documentos.mark-43.net/mark43-service/v1/api-drive/obtener-documentos/2/${data}`;
            var request = require('request');
            request(url, function (error, response, body) {
                let pdfBase64: string = JSON.parse(body)[0];
                res.writeHead(200, {
                    'Content-Type': 'application/pdf',
                    'Content-Disposition': 'attachment; filename=AhorraSeguros-Poliza.pdf'
                });
                return res.end(pdfBase64, 'base64');
            });

        } catch (e) {
            console.log(e);
            return res.status(500).json({
                ok: false,
                msg: 'Error'
            });
        }
    };
}
