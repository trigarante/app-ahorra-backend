import 'reflect-metadata';
import {createConnection} from 'typeorm';
import * as express from 'express';
import routes from './routes/mainRoute';
import * as bodyParser from 'body-parser';

//Connects to the Database -> then starts the express
createConnection().then(async () => {
    // Create a new express application instance
    const app = express();
    app.use(bodyParser.json());
    app.use('/', routes);

    app.listen(8080, () => console.log('Using port: 8080'));
}).catch(error => console.log(error));
