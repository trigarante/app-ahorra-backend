import {Column, Entity, PrimaryColumn} from 'typeorm';

@Entity('registroAppView')
export class RegistroAppView {
    @PrimaryColumn()
    id: number;

    @Column()
    idCliente: number;

    @Column()
    poliza: string;

    @Column()
    primaNeta: number;

    @Column()
    fechaRegistro: Date;

    @Column()
    fechaInicio: Date;

    @Column()
    tipoPago: string;

    @Column()
    cantidadPagos: number;

    @Column()
    tipoSubramo: string;

    @Column()
    numeroSerie: string;

    @Column()
    modelo: string;

    @Column()
    marca: string;

    @Column()
    numeroMotor: string;

    @Column()
    numeroPlacas: string;

    @Column()
    descripcion: string;

    @Column()
    archivo: string;
}
