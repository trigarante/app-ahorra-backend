import {Column, Entity, PrimaryColumn} from 'typeorm';

@Entity('clienteAppView')
export class ClienteAppView {
    @PrimaryColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    paterno: string;

    @Column()
    materno: string;

    @Column()
    fechaNacimiento: Date;

    @Column()
    curp: string;

    @Column()
    rfc: string;

    @Column()
    telefonoMovil: string;

    @Column()
    correo: string;

    @Column()
    pais: string;

    @Column()
    colonia: string;

    @Column()
    cp: number;

    @Column()
    calle: string;

    @Column()
    numExt: string;

    @Column()
    numInt: string;
}
