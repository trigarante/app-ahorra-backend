import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity('clienteBasadaEnProduccion')
export class ClienteAuth {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    contrasenaApp: string;

    @Column()
    curp: string;

    @Column()
    acceso: number;
}
