import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import jwtConfig from '../../config/JWTconfig';

const middleware = express.Router();
const app = express();
app.set('key', jwtConfig.pass);

middleware.use((req, res, next) => {
    const token = req.headers['access-token'];

    if (token) {
        jwt.verify(token, app.get('key'), (err, decode) => {
            if (err) {
                return res.status(401).send({
                    mensaje: err.message,
                });
            } else {
                let contrasenaApp = req.body.contrasenaApp;

                if (contrasenaApp && contrasenaApp.length >= 8) {
                    req['decode'] = decode;
                    req['newPass'] = contrasenaApp
                    next();
                } else {
                    return res.status(500).json({
                        ok: false,
                        msg: 'Error en los datos enviados'
                    });
                }
            }
        });
    } else {
        return res.status(401).send({
            mensaje: 'No tiene token de acceso'
        });
    }
});

export default middleware;
