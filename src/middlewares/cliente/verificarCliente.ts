import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import jwtConfig from '../../config/JWTconfig';

const middlewareCliente = express.Router();
const app = express();
app.set('key', jwtConfig.pass);

middlewareCliente.use((req, res, next) => {
    const token = req.headers['access-token'];

    if (token) {
        jwt.verify(token, app.get('key'), (err, decode) => {
            if (err) {
                return res.status(401).send({
                    mensaje: err.message,
                });
            } else {
                req['id'] = decode.id;
                next();
            }
        });
    } else {
        return res.status(401).send({
            mensaje: 'No tienes token de acceso'
        });
    }
});

export default middlewareCliente;
