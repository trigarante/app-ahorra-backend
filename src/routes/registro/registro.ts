import {Router} from 'express';
import RegistroController from '../../controllers/registro/RegistroController';
import middlewareCliente from '../../middlewares/cliente/verificarCliente';

const router = Router();

router.get('', middlewareCliente, RegistroController.getByIdCliente);
router.get('poliza', middlewareCliente, RegistroController.downloadPdf);

export default router;
