import {Router} from 'express';
import AuthController from '../../controllers/login/AuthController';
import middleware from '../../middlewares/login/checkLogin';

const router = Router();

router.get('/login', AuthController.login);
router.put('/update-pass', middleware ,AuthController.updatePass);

export default router;
