import {Router} from 'express';
import middlewareCliente from '../../middlewares/cliente/verificarCliente';
import ClienteController from '../../controllers/cliente/ClienteController';

const router = Router();

router.get('', middlewareCliente, ClienteController.getById);

export default router;
