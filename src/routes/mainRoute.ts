import {Router} from 'express';
import auth from './login/auth';
import cliente from './cliente/cliente';
import registro from './registro/registro';

const routes = Router();

routes.use('/auth', auth);
routes.use('/cliente', cliente);
routes.use('/registro', registro)

export default routes;
